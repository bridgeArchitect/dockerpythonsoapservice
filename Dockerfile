FROM python:2

WORKDIR /user/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . . 
CMD ["python", "./Task/__init__.py"]
